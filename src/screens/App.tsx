import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import GreetingMoneyScreen from './greetingmoneyscreen/GreetingMoneyScreen';
import {NavigationContainer} from '@react-navigation/native';
import PopupScreen from './popupscreen/PopupScreen';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="PopupScreen" component={PopupScreen} />
        <Stack.Screen
          name="GreetingMoneyScreen"
          component={GreetingMoneyScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
