import React, {useCallback} from 'react';
import {SafeAreaView, Text, TouchableOpacity, View} from 'react-native';
import {StyleSheet} from 'react-native';

const GreetingMoneyScreen = ({navigation}: {navigation: any}) => {
  const onGoBack = useCallback(() => {
    navigation.goBack();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity onPress={onGoBack}>
        <Text>GreetingMoneyScreen</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default GreetingMoneyScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
