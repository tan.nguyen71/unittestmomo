import React, {useCallback, useState} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {BANNER_POPUP_CHUYEN_TIEN_MUNG, IC_CLOSE} from '../../assets';
import styles from './PopupScreenStyle';

const SENDER_NAME = 'NGUYỄN NGỌC ANH';
const PopupScreen = ({navigation}: {navigation: any}) => {
  const [sendername, setSendername] = useState<string>(SENDER_NAME);
  const [isShowPopup, setIsShowPopup] = useState<boolean>(false);

  const onShowPopup = useCallback(() => {
    console.log('onShowPopup');
    setIsShowPopup(true);
  }, []);

  const onClosePopup = useCallback(() => {
    console.log('onClosePopup');
    setIsShowPopup(false);
  }, []);

  const onOpenGreetingMoney = useCallback(() => {
    console.log('onOpenGreetingMoney');
    navigation.navigate('GreetingMoneyScreen');
  }, []);

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={onShowPopup}
        style={styles.showPopupTouch}
        testID="showPopup">
        <Text>Show Pop up</Text>
      </TouchableOpacity>
      {isShowPopup && (
        <View style={styles.backgroundPopup} testID="popup">
          <TouchableOpacity
            onPress={onOpenGreetingMoney}
            testID="openGreetingMoney">
            <Image
              source={BANNER_POPUP_CHUYEN_TIEN_MUNG}
              style={styles.popup}
            />
          </TouchableOpacity>
          <View style={styles.greetingMoneyTextView}>
            <Text style={styles.sendernameText}>{sendername}</Text>
            <Text style={styles.greetingMoneyText}>
              vừa chuyển tiền mừng cho bạn
            </Text>
          </View>
          <TouchableOpacity onPress={onClosePopup} testID="closePopup">
            <Image source={IC_CLOSE} style={styles.icClose} />
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

export default PopupScreen;
