import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
  },
  backgroundPopup: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    alignItems: 'center',
  },
  showPopupTouch: {
    position: 'absolute',
    alignSelf: 'center',
  },
  popup: {
    height: 375,
    width: 375,
    marginTop: 199,
  },
  icClose: {
    marginTop: 16,
  },
  sendernameText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
  },
  greetingMoneyText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'center',
    marginTop: 5,
  },
  greetingMoneyTextView: {
    position: 'absolute',
    top: 468,
    alignItems: 'center',
  },
});

export default styles;
