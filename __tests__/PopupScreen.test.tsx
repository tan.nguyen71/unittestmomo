import 'react-native';
import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import PopupScreen from '../src/screens/popupscreen/PopupScreen';
import renderer from 'react-test-renderer';

const SENDER_NAME = 'NGUYỄN NGỌC ANH';
describe('Popup screen', () => {
  it('renders correctly', () => {
    const screen = renderer
      .create(<PopupScreen navigation={() => {}} />)
      .toJSON();
    expect(screen).toMatchSnapshot();
  });

  it('should show popup', () => {
    const navigation = {navigate: () => {}};
    const screen = render(<PopupScreen navigation={navigation} />);

    fireEvent.press(screen.getByTestId('showPopup'));

    expect(screen.getByTestId('popup')).toBeTruthy();
    expect(screen.getByText(SENDER_NAME)).toBeTruthy();
    expect(screen.getByText('vừa chuyển tiền mừng cho bạn')).toBeTruthy();
  });

  it('should close popup', () => {
    const navigation = {navigate: () => {}};
    const screen = render(<PopupScreen navigation={navigation} />);

    fireEvent.press(screen.getByTestId('showPopup'));
    fireEvent.press(screen.getByTestId('closePopup'));

    expect(screen.queryByTestId('popup')).toBe(null);
    expect(screen.queryByText(SENDER_NAME)).toBe(null);
    expect(screen.queryByText('vừa chuyển tiền mừng cho bạn')).toBe(null);
  });

  // it('should go to GreetingMoneyScreen', () => {
  //   const navigation = {navigate: () => {}};
  //   spyOn(navigation, 'navigate');
  //   const {getByTestId} = render(<PopupScreen navigation={navigation} />);

  //   fireEvent.press(getByTestId('showPopup'));
  //   fireEvent.press(getByTestId('openGreetingMoney'));

  //   expect(navigation.navigate).toHaveBeenCalledWith('GreetingMoneyScreen');
  // });
});
