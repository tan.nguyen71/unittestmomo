# REPORT

## Cài đặt

Cài dependency

```
yarn add --dev jest
yarn add --dev @testing-library/react-native
```

config file `package.json`

```
"jest": {
    "preset": "react-native",
    "transformIgnorePatterns": ["node_module/(?!@react-native|react-native)"],
    ...
  }
```

Tạo file `PopupScreen.test.tsx` cho màn `PopupScreen.tsx` trong folder `__tests__`,
viết test trong file `PopupScreen.test.tsx`

Chạy test

```
yarn test --watch
```

## Mô tả testcase

- Test render của screen.

```
  it('renders correctly', () => {
    const screen = renderer
      .create(<PopupScreen navigation={() => {}} />)
      .toJSON();
    expect(screen).toMatchSnapshot();
  });
```

---

- Test popup có show không và show có đúng không.

```
  it('should show popup', () => {
    const navigation = {navigate: () => {}};
    const screen = render(<PopupScreen navigation={navigation} />);

    fireEvent.press(screen.getByTestId('showPopup'));

    expect(screen.getByTestId('popup')).toBeTruthy();
    expect(screen.getByText(SENDER_NAME)).toBeTruthy();
    expect(screen.getByText('vừa chuyển tiền mừng cho bạn')).toBeTruthy();
  });
```

---

- Test khi ấn icon close, popup có đóng hay không.

```
  it('should close popup', () => {
    const navigation = {navigate: () => {}};
    const screen = render(<PopupScreen navigation={navigation} />);

    fireEvent.press(screen.getByTestId('showPopup'));
    fireEvent.press(screen.getByTestId('closePopup'));

    expect(screen.queryByTestId('popup')).toBe(null);
    expect(screen.queryByText(SENDER_NAME)).toBe(null);
    expect(screen.queryByText('vừa chuyển tiền mừng cho bạn')).toBe(null);
  });
```
